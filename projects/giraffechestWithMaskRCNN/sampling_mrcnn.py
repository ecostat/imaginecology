import glob, os

# Current directory - REPLACE BY YOUR COMPLETE PATH
images_dir = "images_resized/" #directory of the images
annotations_dir = "annotations_xml/" #directory of the annotations
path_data = 'dataset/' #where the images and annotations will reside now
percentage_test = 10 #percentage of images going in the validation dataset

# Directory where the data will reside  - REPLACE BY YOUR COMPLETE PATH
def make_dir(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
        print("create {} directory".format(directory))

make_dir(path_data+"train/images/")
make_dir(path_data+"val/images/")
make_dir(path_data+"train/annots/")
make_dir(path_data+"val/annots/")

# move the files in the right folders
img_ext=".jpg" #sometimes .jpg sometimes .JPG...
counter = 1
index_test = round(100 / percentage_test)

for pathAndFilename in glob.iglob(os.path.join(images_dir, "*"+img_ext)):
    title, ext = os.path.splitext(os.path.basename(pathAndFilename))

    if os.path.exists(annotations_dir+title+".xml"): #some image are not annotated
        print("move ",title,img_ext)
    else:
        print("skip (no .xml)",title,img_ext)
        continue

    if counter == index_test:
        counter = 1
        os.rename(images_dir+title+img_ext,path_data+"/val/images/"+title+img_ext)
        os.rename(annotations_dir+title+".xml",path_data+"/val/annots/"+title+".xml")
    else:
        os.rename(images_dir+title+img_ext,path_data+"/train/images/"+title+img_ext)
        os.rename(annotations_dir+title+".xml",path_data+"/train/annots/"+title+".xml")
        counter = counter + 1
