import glob, os

# Current directory - REPLACE BY YOUR COMPLETE PATH
current_dir = "training/"

# Directory where the data will reside  - REPLACE BY YOUR COMPLETE PATH
path_data = 'training/'

# Percentage of images to be used for the test set
percentage_test = 10;

# Create and/or truncate train.txt and test.txt
file_train = open('train.txt', 'w')  
file_test = open('test.txt', 'w')

# Populate train.txt and test.txt
counter = 1
percentage_test = 10
index_test = round(100 / percentage_test)  
for pathAndFilename in glob.iglob(os.path.join(current_dir, "*.jpg")):  
    title, ext = os.path.splitext(os.path.basename(pathAndFilename))

    if counter == index_test:
        counter = 1
        file_test.write(path_data + "/" + title + '.jpg' + "\n")
    else:
        file_train.write(path_data + "/" + title + '.jpg' + "\n")
        counter = counter + 1
