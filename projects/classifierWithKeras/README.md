# Image Classification with Keras

by Gaspard Dussert & Vincent Miele  ([CNRS/LBBE](https://lbbe.univ-lyon1.fr/))

A classifier learn how to automatically predict the class of the object inside  an image for a given list of class. Images should **contain only one object** because the classifier will classify any image as a whole, not searching a particular object into the image.   It could be therfore necessary to pre-process the images and crop around objects with an object detector (see [others sections](../README.md)).

## 0. Dataset used in this tutorial

<center>
<img src="../../images/bluebell.jpg " height="224">
<img src="../../images/cowslip.jpg " height="224">
<img src="../../images/tigerlily.jpg " height="224">
</center>

In this tutorial, we use the publicy available [Oxford Flowers](http://www.robots.ox.ac.uk/~vgg/data/flowers/) dataset: 17 flower species with 80 images per species.

Since the training dataset is quite small, it is impossible to learn a model directly. The trick, aka [transfer learning](https://machinelearningmastery.com/transfer-learning-for-deep-learning/) with CNN, consists in reusing a pre-trained model as the starting point for a model on a particular task of interest.

<center>
<img src="../../images/transferlearning.jpeg" width="400">
</center>

In this tutorial the model will be pre-trained on the ImageNet dataset, the most famous one for classification. ImageNet has more than 14 000 000 images and models trained on it have already learned some general features that will help to classify our 17 flowers. 

### 0.1. Organize the dataset

Instead of having one folder with all the images, for the following steps of the tutorial we will need one directory for each class of the dataset. For example :

```bash
dataset
├── class_1
│   ├── image_1
│   ├── image_2
│   ├── image_3
├── class_2
│   ├── image_1
│   ├── image_2
....
```

The flower dataset organized in folders can be downloaded [here](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/classifier/).

## 1. Installation
In this tutorial, we rely on one of the hottest frameworks for deep learning, [Keras](https://keras.io), which has been proved to be much simplier than other frameworks including [PyTorch](https://pytorch.org/ ) which is plebiscitated by machine learning specialists. 

For this tutorial you just need the last version of Python3, [Tensorflow](https://www.tensorflow.org/install) (see the [GPU Guide](https://www.tensorflow.org/install/gpu) for GPU support) and [Keras](https://keras.io/#installation).

## 2. The Model

Keras offers several pre-trained models such as ResNet, VGG, Xception. The full list can be found  on the [Applications](https://keras.io/applications/) page of Keras. 

For this tutorial you can use any of them. Here we use Xception as an example. 

**NB :** The full code that we will detail in the following is available in [classifier.py](classifier.py). It is ready to run!

**NB2 : If you don't have a GPU but want to do the training anyway, try with MobileNet instead of Xception: it is a smaller model that will train faster and run on your CPU.** 

It is important to know that the architecture of a classifier can be divided in two parts : the feature learning part (convolution layers) and the classification part (fully connected layers). When using transfer learning, it is possible to keep the feature learning part that will help us to extract good features. However, the classification part is specific to the task it has been trained for (ImageNet classes) and must be modified (i.e. trained again).

<center>
<img src="../../images/CNN.jpeg" width="800">
</center>

To make this in Keras, the first step is to import the model : 

```python
from keras.applications.xception import Xception, preprocess_input
```

(Every model comes with a `preprocess_input` function : when an image is loaded it is an array of values between 0 and 255, before feeding it to the model for training or prediction some transformation must be made (rescaling, normalization, etc.), `preprocess_input` apply all these transformations directly. The transformations can be different from one model to another). 

Now we can create an instance of the model, without the classification part ( `include_top=False`) and with a correct input shape (which can be found on the [Applications](https://keras.io/applications/) page).  

```python
base_model = Xception(include_top=False, weights="imagenet",input_shape=(299,299,3))
```

There are many ways to create a classification part... We keep simple with one Global Average Pooling layer, two Dense layers and a Softmax activation (see our [glossary](../../doc/README.md) for more explanations). The last Dense layer must have the same size as our number of class : 17.

```python
from keras.layers import Dense,GlobalAveragePooling2D, Activation
x = base_model.output
x = GlobalAveragePooling2D()(x)
x = Dense(512)(x)
x = Dense(17)(x)
preds = Activation("softmax")(x)
```

The full model can be build using the function `Model` :

```python
from keras.models import Model
model = Model(inputs=base_model.input,outputs=preds)
```

The final step is to compile the model: here, a loss function and an optimizer is needed. 

For classification problems, the Categorical Cross-Entropy loss is generally used (see this [page](https://gombru.github.io/2018/05/23/cross_entropy_loss/) for definition and explanations). 

The optimizer is what control the learning rate: here, we use the most standard one, SGD (Stochastic Gradient Descent) with default parameters. See this [page](https://keras.io/optimizers/) for the list of optimizers available in Keras. 

We also add the accuracy XXXX ds la glossaire XXX to the list of metrics that are being computed during the training.

```python
model.compile(optimizer = "sgd", loss = 'categorical_crossentropy',metrics = ['accuracy'])
```

## 3. Loading the data

Loading the data in Keras is facilitated by the use of *generators*. It can be done it two steps. 

- First, create an instance of `ImageDataGenerator` with the `preprocessing_function` and the relative size of the validation dataset as parameters :

```python
from keras.preprocessing.image import ImageDataGenerator
data_generator = ImageDataGenerator(preprocessing_function = preprocess_input,
                                   validation_split = 0.1)
```

- Use the method `flow_from_directory` that automatically find the classes in your formatted dataset directory.  You also have to specify a batch size (for example 16) and the target size of the images (the `input_shape` of the model defined earlier) : 

```python
batch_size = 16

train_generator = data_generator.flow_from_directory(directory = "Flowers/",
     batch_size = batch_size,
     class_mode = 'categorical',
     target_size = (299, 299),
     subset = 'training')

validation_generator = data_generator.flow_from_directory(directory = "Flowers/",
     batch_size = batch_size,
     class_mode = 'categorical',
     target_size = (299, 299),
     subset = 'validation')
```

**NB :** using a generator can slow down the training compared to directly loading all the data in memory. However the latter option is feasible only when dataset is small and it is not convenient to achieve data augmentation (see below).

## 4. Training the model

Everything is ready for the training, we just need to choose the number of epochs, let's try with 10 : 

```python
model.fit(train_generator, validation_data = validation_generator, epochs = 10)
```

```
Epoch 1/10
77/77 [==============================] - 61s 791ms/step - loss: 2.0387 - accuracy: 0.5466 - val_loss: 1.2333 - val_accuracy: 0.7353
Epoch 2/10
77/77 [==============================] - 42s 547ms/step - loss: 0.7792 - accuracy: 0.9069 - val_loss: 0.3329 - val_accuracy: 0.8603
Epoch 3/10
77/77 [==============================] - 42s 547ms/step - loss: 0.3339 - accuracy: 0.9690 - val_loss: 0.0833 - val_accuracy: 0.9559
Epoch 4/10
77/77 [==============================] - 43s 553ms/step - loss: 0.2151 - accuracy: 0.9763 - val_loss: 0.1788 - val_accuracy: 0.9485
Epoch 5/10
77/77 [==============================] - 42s 549ms/step - loss: 0.1400 - accuracy: 0.9869 - val_loss: 0.2793 - val_accuracy: 0.9559
Epoch 6/10
77/77 [==============================] - 42s 549ms/step - loss: 0.0988 - accuracy: 0.9918 - val_loss: 0.0881 - val_accuracy: 0.9706
Epoch 7/10
77/77 [==============================] - 42s 550ms/step - loss: 0.0695 - accuracy: 0.9935 - val_loss: 0.3844 - val_accuracy: 0.9706
Epoch 8/10
77/77 [==============================] - 42s 548ms/step - loss: 0.0580 - accuracy: 0.9967 - val_loss: 0.0451 - val_accuracy: 0.9632
Epoch 9/10
77/77 [==============================] - 42s 550ms/step - loss: 0.0479 - accuracy: 0.9967 - val_loss: 0.1397 - val_accuracy: 0.9632
Epoch 10/10
77/77 [==============================] - 42s 551ms/step - loss: 0.0466 - accuracy: 0.9975 - val_loss: 0.5133 - val_accuracy: 0.9706
```

We can see that the validation accuracy increases until epoch 6 and then stabilizes around 97%: this is pretty good !

## 5. Saving the weights

To save the weights after the training you can do :

```python
model.save_weights("weights.hdf5")
```

However, in this case, we only consider the weights of the last epoch... To save the weights of all epochs, a `callback` is needed. **Callbacks are functions that are called at the end of each epoch or each batch.**
Here we use `ModelCheckpoint` that is called at the end of each epoch to save the model weights in `weights-xxx.hdf5` where `xxx` is the epoch number.

```python
from keras.callbacks import ModelCheckpoint

weightpath = "weights" + "-{epoch:03d}.hdf5"
checkpoint = ModelCheckpoint(weightpath)
```

Don't forget to add `checkpoint` to the callbacks list during the training :

```python
model.fit(train_generator, validation_data = validation_generator, epochs = 10, callbacks = [checkpoint])
```

**NB :** [Here](https://keras.io/callbacks/) is the list of all Keras Callbacks (such as Tensorboard). It is also possible to make a custom callback.

The weights can then be loaded after creating the model (with `$WEIGHT_PATH` the path to the model weights) :

```
model.load_weights($WEIGHT_PATH)
```


## 6. Use the model for prediction

For this part, you can load the weights of the epoch with the highest validation accuracy or keep the last epoch.

We want to test our model... however, we didn't made a test set!! Let's try with some freely available images that are in the `test/` folder (you can also try with your own test images). 
XXX jeu de test/train/validation ds gloassaireXXX

Here is a code to load an image and predict its class using the trained model :

```python
from keras.preprocessing import image
import numpy as np

#classes names (found by flow_from_directory)
labels = (train_generator.class_indices)
labels = dict((v,k) for k,v in labels.items())

def predict(filename):
     img = image.load_img(filename, target_size=(299,299)) #load image
     x = image.img_to_array(img) #convert it to array
     x = np.expand_dims(x, axis=0) #simulate batch dimension
     x = preprocess_input(x) #preprocessing
     pred = model.predict(x) #classes prediction
     class_pred = labels[np.argmax(pred)] #find class with highest confidence
     conf = pred[0,np.argmax(pred)] #confidence
     print(f"Class : {class_pred} ({round(100*conf,1)}%)")
```

We now consider the image `test/max-conrad-9nkP0aerrN4-unsplash.jpg` :

<center>
<img src="test/max-conrad-9nkP0aerrN4-unsplash.jpg" height="224">
</center>

```python
predict("test/max-conrad-9nkP0aerrN4-unsplash.jpg")
```

Output : 

```
Class : Snowdrop (99.5%)
```

**NB :** It is also possible to make a test generator using `flow_from_directory` and then evaluate the model on the whole test set using `model.evalutate()`.


## 7. Bonus 

### 7.1. Data augmentation

Data augmentation is a technique that increases artificially the number of image there are in the dataset. For example flipping horizontally the image of a flower produces an new image for the flower's species. 

Keras generators make it possible to do data augmentation easily: to add horizontal flipping and random rotation, use the following `ImageDataGenerator` instead of the one used previously in `3.`

```python
data_generator = ImageDataGenerator(preprocessing_function = preprocess_input,
                                   validation_split = 0.1,
                                   horizontal_flip = True,
                                   rotation_range = 10)
```

It should improve the accuracy by 1% or 2% on the flower dataset using this data augmentation.

Here is [the list of data augmentation](https://keras.io/preprocessing/image/) that can be done with Keras generators. 

### 7.2. Custom model and training from scratch

Training a model from scratch on this dataset is not reasonnable because the number of images is too low. However, let's see how we would do it anyway : 

Using a `Sequential` model, we can define the layers one by one. We propose a very basical architecture with 4 convolution layers followed by a ReLu activation and a Max Pooling. The classification part is the same as before. 

Remember that the first layer need one more parameter : `input_shape`, here we set it to `128x128` to have less parameters.

```python
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D,GlobalAveragePooling2D, Dense, Activation

model = Sequential()

## Feature Learning Part
model.add(Conv2D(32, (3, 3), padding='same', activation = "relu", input_shape=(128,128,3)))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3), padding='same', activation = "relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(128, (3, 3), padding='same', activation = "relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(256, (3, 3), padding='same', activation = "relu"))

#Classification Part
model.add(GlobalAveragePooling2D())
model.add(Dense(256, activation = "relu"))
model.add(Dense(17))
model.add(Activation('softmax'))
```

And compiling the model, with Adam instead of SGD for faster convergence :

```python
model.compile(optimizer = "adam", loss = 'categorical_crossentropy',metrics = ['accuracy'])
```

Before starting the training, we must also changed the data loading part : we don't have a `preprocessing_function` anymore because we are not using a pre-trained model and we changed the `input_shape` to `(128,128)` so it must also be changed in the `target_size` of the method `flow_from_directory`.

```python
train_datagen = ImageDataGenerator(rescale=1./255,
                                   validation_split = 0.1)
                                   
train_generator = train_datagen.flow_from_directory(directory = "Flowers/",
     batch_size = batch_size,
     class_mode = 'categorical',
     target_size = (128, 128),
     subset = 'training')

validation_generator = train_datagen.flow_from_directory(directory = "Flowers/",
     batch_size = batch_size,
     class_mode = 'categorical',
     target_size = (128, 128),
     subset = 'validation')
```

We added `rescale = 1./255` in order that each image is an array with values between 0 and 1.

Training this model for 100 epochs should give a validation accuracy around 75%.

