#### Deep Learning Framework

[Keras]([https://keras.io/) and the R package https://keras.rstudio.com/ ](keras)

[TensorFlow](https://www.tensorflow.org), [pyTorch](https://pytorch.org) and [MXNet](https://mxnet.apache.org/)



#### General purpose object detection

(for an introduction about this section, see [this page](https://towardsdatascience.com/r-cnn-fast-r-cnn-faster-r-cnn-yolo-object-detection-algorithms-36d53571365e))

[YOLO](https://github.com/AlexeyAB/darknet) In YOLO a single convolutional network predicts the bounding boxes and the class probabilities for these boxes. Widely used, in particular with animals as "object" to detect. [arXiv preprint](papers/ieee-redmon2016_yolo.pdf)

> Tutorial by @vm-lbbe and @Gasp34 [available here](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/giraffechestWithYolo).

[Mask-R-CNN](https://github.com/matterport/Mask_RCNN) The model generates bounding boxes and segmentation masks (a *mask* is the exact contours of an objects) of objects in the image. The mask detection requires a training on annotated images with the contours of all objects (animals for instance). [arXiv preprint](papers/arxiv-he2018_mask-R-CNN.pdf)

> Tutorial by @vm-lbbe and @Gasp34 [available here](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/giraffechestWithMaskRCNN).

[RetinaNet](https://github.com/fizyr/keras-retinanet) New loss called Focal Loss to correct the problem of imbalance between foreground and background classes. [arXiv preprint](papers/arxiv-lin2018_focal_loss.pdf)

>Tutorial by @vm-lbbe and @Gasp34 [available here](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/cameraTrapDetectionWithRetinanet).

#### Camera traps

[Camera Trap Classifier](https://github.com/marco-willi/camera-trap-classifier) This repository contains code and documentation to train and apply CNN for identifying animal species in photographs from camera traps. Code in Python and based on Tensorflow.
> Tutorial  by @vm-lbbe and @Gasp34 [available here](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/classificationWithCameraTrapClassifier).

[Microsoft codes](https://www.microsoft.com/en-us/research/project/accelerating-image-based-biodiversity-surveys/) including a too dedicated to camera traps with [Megadetector](https://github.com/microsoft/CameraTraps)
One-class animal detector (i.e. detects any animal but does not classify) trained on several hundred thousand bounding boxes from a variety of ecosystems.

> Tutorial by @vm-lbbe and @Gasp34 [available here](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/detectionWithMegaDetector).

[MLWIC2: Machine Learning for Wildlife Image Classification in R](https://github.com/mikeyEcology/MLWIC2)
This package identifies animal species in camera trap images by implementing the model described in [Tabak et al, MEE 2018](https://gitlab.com/ecostat/imaginecology/-/tree/master/papers/mee-tabak2018_cameratrap_tensorflow.pdf) and updated in [Tabak et al,biorxiv 2020](https://gitlab.com/ecostat/imaginecology/-/tree/master/papers/biorxiv-tabak2020_MLWIC2.pdf) Linux, Mac, (Windows)



[DLCTI](https://github.com/Evolving-AI-Lab/deep_learning_for_camera_trap_images)
Along with [Norouzzadeh, PNAS 2018](https://gitlab.com/ecostat/imaginecology/-/tree/master/papers/pnas-norouzzadeh2018_identif_count_animals.pdf)
> Deprecated

[Animal Scanner]() Software for classifying humans, animals, and empty frames in camera trap images.
Along with [Yousif et al, Eco Evo 2019](https://gitlab.com/ecostat/imaginecology/-/tree/master/papers/ecoevo-yousif2019_animalscanner_software.pdf)
> Deprecated

#### Face recognition
[face_recognition](https://github.com/ageitgey/face_recognition)
Recognize and manipulate faces from Python.

#### Depth estimation

[Monodepth2](https://github.com/nianticlabs/monodepth2)

Training and testing depth estimation model

#### Object Tracking

[SORT](https://github.com/nwojke/deep_sort)

Simple Online and Realtime Tracking with a Deep Association Metric

#### Unsupervised clustering
[DeepCluster](https://github.com/facebookresearch/deepcluster)
Clustering for Unsupervised Learning of Visual Features.

#### Model interpretation
[tf-explain](https://github.com/sicara/tf-explain)
Interpretability methods as Tensorflow 2.0 callbacks to ease neural network's understanding.

[Model interpretability with Integrated Gradients in Keras](https://keras.io/examples/vision/integrated_gradients/)
Integrated Gradients is a technique for attributing a classification model's prediction to its input features.