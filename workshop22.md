Le GdR EcoStat organise un **2ème workshop "imaginecology"** sur le thème du 
"Deep Learning pour le traitement d'image (ou du son) en écologie".

Ce sera les **29 et 30 novembre** 2022.

Plus d'informations ici :  [https://imaginecology2.sciencesconf.org/](https://imaginecology2.sciencesconf.org/)

*Sakina-Dorothée Ayata, Olivier Dézerald, Stéphane Dray, Guglielmo Fernandez Garcia, Olivier Gimenez, François Martignac, Vincent Miele et Julien Renoult*
