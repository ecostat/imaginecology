# A non-exhaustive list of annotation tools 

From our discussions during the `imaginecology` workshop in 2022.

(alphabetical order)

### [Biigle](https://biigle.de/)
BIIGLE is a **web service** for the efficient and rapid annotation of still images and videos. It was built for **marine** environmental monitoring.

### [CVAT (Computer Vision Annotation Tool),](https://www.cvat.ai/)
Proposes multiple annotation, auto-annotation, tracking for image and video. Data **in the cloud** or **install via opencv**.

### [VGG Image Annotator (VIA)](https://www.robots.ox.ac.uk/~vgg/software/via/)
VGG Image Annotator is a simple and standalone manual annotation software for image, audio and video. VIA **runs in a web browser** and does not require any installation or setup.

### [Label Studio](https://github.com/heartexlabs/label-studio)
Label Studio is an **open source** data labeling tool for audio, text, images, videos, and time series.

### [LabelMe](https://github.com/wkentaro/labelme)
Labelme is a graphical image annotation tool. It is **standalone Python program** and uses Qt for its graphical interface.

### [LabelBox](https://labelbox.com/)
End-to-end **platform**.

### [Ocapi](https://ocapi.terroiko.fr)
(in french) Plateforme d'annotation française. L'utilisateur transmet la propriété intelectuelle de ses images (cf. CGU).

### [Prodigy by spaCy](https://prodi.gy)
Non free. An annotation tool powered by **active learning**. Prodigy is a scriptable annotation tool so efficient that data scientists can do the annotation themselves, enabling a new level of rapid iteration.

### [RobotFlow](https://roboflow.com/)
**Upload files** including images, annotations, and videos. Label using any operating system without downloading any software. Use the most popular annotation formats including JSON, XML, CSV, and TXT.

### [SQUIDLE+](https://squidle.org/)
A centralised **marine** image data management.

### [Zooniverse](https://www.zooniverse.org/)
Zooniverse gives **people** of all ages and backgrounds the chance to **participate in sciences**.

