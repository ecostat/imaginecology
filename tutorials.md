# Do you want to try? step-by-step tutorials for beginners

The following tutorials deal with two independent tasks that have different requirements:

* all about **training a new CNN model**, with a dataset on your own, is highly demanding (i.e. it requires a large amount of calculations). In most cases it requires:
	* a GPU card and all its ecosystem (drivers, CUDA installed,...)
	* a range of software librairies to install on the system (cuDNN, TensorFlow,...)
	* mastering the OS, preferentially Linux
 * **using a pre-trained model** is much less demanding, does not necessarily requires a GPU card and is much much easier.

In these tutorials, it is possible to skip the training part (when there is one) since the pre-trained models are made available.

NOTA BENE: it should take just the snap of a finger to re-run these analysis on **Linux** (the widespread OS in the machine learning community). However, some changes/adaptations are required on Windows

---
### [Tutorial 1: Detecting giraffe flanks with YOLO (deprecated)](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/giraffechestWithYolo/) (training + detection)

<center>
<img src="images/giraffe_yolo.png" height="200">
</center>

Warning: using Yolo on Windows is highly challenging whereas possible in theory

---
### [Tutorial 2: Detecting giraffe flanks with Mask-RCNN](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/giraffechestWithMaskRCNN/) (training + detection)

<center>
<img src="images/show_annot.png" height="200">
</center>

---
### [Tutorial 3: Detecting any animal without classification with MegaDetector](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/detectionWithMegaDetector/) (detection)

<center>
<img src="images/snake.jpg " height="200">
</center>

---
### [Tutorial 4: Classifying camera trap images with Camera Trap Classifier](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/classificationWithCameraTrapClassifier/) (classification)

<center>
<img src="images/ctc_csv.png " height="200">
</center>

Warning: varying classification performance...

---
### [Tutorial 5: Classifying camera trap images with RetinaNet](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/cameraTrapDetectionWithRetinanet/) (training + detection/classification)

<center>
<img src="images/retinanet_reconyx2.png" height="200">
</center>

---
### [Tutorial 6: Visual Explanations of a CNN with Grad-CAM](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/gradcamForClassifier/) (detection/classification + post-treatment)

<center>
<img src="images/gradcam_zebra.png" height="200">
</center>

---
### [Tutorial 7: Classifying flower images with Keras](https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/classifierWithKeras/) (training + classification)

<center>
<img src="images/bluebell.jpg " height="124">
<img src="images/cowslip.jpg " height="124">
<img src="images/tigerlily.jpg " height="124">
</center>
